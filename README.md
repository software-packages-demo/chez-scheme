# chez-scheme

Scheme Compiler [cisco/ChezScheme](https://github.com/cisco/ChezScheme)

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc+ocaml-base-nox+ghc+sbcl+julia+scala+racket+elixir+clojure+chezscheme&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%Y&beenhere=1)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=ecl+maxima-sage+racket+clojure+chezscheme+smlnj+picolisp+polyml&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%Y)
